# SPDX-FileCopyrightText: 2021-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: LGPL-3.0-only

# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import os.path as osp
import sys
import warnings
from pathlib import Path
from typing import Optional

# note: we need to import pyplot here, because otherwise it might fail to load
# the ipython extension
import matplotlib.pyplot as plt  # noqa: F401
from docutils import nodes
from docutils.parsers.rst import directives
from docutils.parsers.rst.directives import images
from docutils.statemachine import StringList
from sphinx.ext import apidoc
from sphinx.util.docutils import SphinxDirective

sys.path.insert(0, os.path.abspath(".."))

warnings.filterwarnings("ignore", message=r"\s*Downloading:")

if not os.path.exists("_static"):
    os.makedirs("_static")

# isort: off

import psy_view

# isort: on


def generate_apidoc(app):
    appdir = Path(app.__file__).parent
    apidoc.main(
        ["-fMEeTo", str(api), str(appdir), str(appdir / "migrations" / "*")]
    )


api = Path("api")

if not api.exists():
    generate_apidoc(psy_view)

# -- Project information -----------------------------------------------------

project = "psy-view"
copyright = "2021-2024 Helmholtz-Zentrum hereon GmbH"
author = "Philipp S. Sommer"


linkcheck_ignore = [
    # we do not check link of the psy-view as the
    # badges might not yet work everywhere. Once psy-view
    # is settled, the following link should be removed
    r"https://.*psy-view"
]


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "hereon_nc_sphinxext",
    "sphinx.ext.intersphinx",
    "sphinx_design",
    "sphinx.ext.todo",
    "autodocsumm",
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.viewcode",
    "IPython.sphinxext.ipython_console_highlighting",
    "IPython.sphinxext.ipython_directive",
    "sphinxarg.ext",
    "psyplot.sphinxext.extended_napoleon",
]

rebuild_screenshots = False

confdir = osp.dirname(__file__)


# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]


autodoc_default_options = {
    "show_inheritance": True,
    "members": True,
    "autosummary": True,
}


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = "sphinx_rtd_theme"

html_theme_options = {
    "collapse_navigation": False,
    "includehidden": False,
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]


intersphinx_mapping = {
    "python": ("https://docs.python.org/3/", None),
    "pandas": ("https://pandas.pydata.org/pandas-docs/stable/", None),
    "numpy": ("https://numpy.org/doc/stable/", None),
    "matplotlib": ("https://matplotlib.org/stable/", None),
    "sphinx": ("https://www.sphinx-doc.org/en/master/", None),
    "xarray": ("https://xarray.pydata.org/en/stable/", None),
    "cartopy": ("https://scitools.org.uk/cartopy/docs/latest/", None),
    "psyplot": ("https://psyplot.github.io/psyplot/", None),
    "psy_simple": ("https://psyplot.github.io/psy-simple/", None),
    "psy_maps": ("https://psyplot.github.io/psy-maps/", None),
    "psyplot_gui": ("https://psyplot.github.io/psyplot-gui/", None),
}


def create_screenshot(
    code: str,
    output: str,
    make_plot: bool = False,
    enable: Optional[bool] = None,
    plotmethod: str = "mapplot",
    minwidth=None,
    generate=rebuild_screenshots,
) -> str:
    """Generate a screenshot of the GUI."""
    from psyplot.data import open_dataset
    from PyQt5.QtWidgets import (  # pylint: disable=no-name-in-module
        QApplication,
        QSizePolicy,
    )

    from psy_view.ds_widget import DatasetWidget

    output = osp.join("_static", output)

    app = QApplication.instance()
    if app is None:
        app = QApplication([])

    if not generate and osp.exists(output):
        return output

    ds_widget = DatasetWidget(open_dataset(osp.join(confdir, "demo.nc")))
    ds_widget.plotmethod = plotmethod

    if make_plot:
        ds_widget.variable_buttons["t2m"].click()

    if minwidth:
        ds_widget.setMinimumWidth(minwidth)

    options = {"ds_widget": ds_widget}
    exec("w = " + code, options)
    w = options["w"]

    if enable is not None:
        w.setEnabled(enable)

    w.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)

    ds_widget.show()  # to make sure we can see everything

    w.grab().save(osp.join(confdir, output))
    ds_widget.close_sp()
    ds_widget.close()
    return output


def plotmethod(argument):
    return directives.choice(argument, ("mapplot", "lineplot", "plot2d"))


class ScreenshotDirective(SphinxDirective):
    """A directive to generate screenshots of the GUI.

    Usage::

        .. screenshot:: <widget> <img-file>
            :width: 20px
            ... other image options ...
    """

    has_content = False

    option_spec = images.Image.option_spec.copy()

    option_spec["plot"] = directives.flag
    option_spec["enable"] = directives.flag
    option_spec["plotmethod"] = plotmethod
    option_spec["minwidth"] = directives.positive_int
    option_spec["generate"] = directives.flag

    target_directive = "image"

    required_arguments = 2
    optional_arguments = 0

    def add_line(self, line: str) -> None:
        """Append one line of generated reST to the output."""
        source = self.get_source_info()
        if line.strip():  # not a blank line
            self.result.append(line, *source)
        else:
            self.result.append("", *source)

    def generate(self) -> None:
        """Generate the content."""
        self.add_line(f".. {self.target_directive}:: {self.img_name}")

        for option, val in self.options.items():
            self.add_line(f"    :{option}: {val}")

    def run(self):
        """Run the directive."""
        self.result = StringList()

        make_plot = self.options.pop("plot", False) is None
        enable = True if self.options.pop("enable", False) is None else None

        rebuild_screenshot = (
            self.options.pop("generate", False)
            or self.env.app.config.rebuild_screenshots
        )

        self.img_name = create_screenshot(
            *self.arguments,
            make_plot=make_plot,
            enable=enable,
            plotmethod=self.options.pop("plotmethod", None) or "mapplot",
            minwidth=self.options.pop("minwidth", None),
            generate=rebuild_screenshot,
        )

        self.generate()

        node = nodes.paragraph()
        node.document = self.state.document
        self.state.nested_parse(self.result, 0, node)

        return node.children


class ScreenshotFigureDirective(ScreenshotDirective):
    """A directive to generate screenshots of the GUI.

    Usage::

        .. screenshot-figure:: <widget> <img-file>
            :width: 20px
            ... other image options ...

            some caption
    """

    target_directive = "figure"

    has_content = True

    def generate(self):
        super().generate()

        if self.content:
            self.add_line("")
            indent = "    "
            for line in self.content:
                self.add_line(indent + line)


def setup(app):
    app.add_directive("screenshot", ScreenshotDirective)
    app.add_directive("screenshot-figure", ScreenshotFigureDirective)
    app.add_config_value("rebuild_screenshots", rebuild_screenshots, "env")
    app.add_css_file("theme_overrides.css")
